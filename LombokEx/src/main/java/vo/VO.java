package vo;

import lombok.Data;

@Data
public class VO {
	private String name;
	private int age;
	private boolean gender;
}